package com.aait.wasite.UI.Activities.AppInfo

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.Main.MainActivity
import com.aait.wasite.UI.Activities.SplashActivity

class SettingsActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_settings

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var arabic:Button
    lateinit var english:Button

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        arabic = findViewById(R.id.arabic)
        english = findViewById(R.id.english)

        title.text = getString(R.string.settings)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish() }
        arabic.setOnClickListener { lang.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }
        english.setOnClickListener { lang.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java)) }

    }
}