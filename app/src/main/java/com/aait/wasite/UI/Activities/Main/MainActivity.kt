package com.aait.wasite.UI.Activities.Main

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.GPS.GPSTracker
import com.aait.wasite.GPS.GpsTrakerListener
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.CategoryModel
import com.aait.wasite.Models.HomeResponse
import com.aait.wasite.Models.TermsResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.AppInfo.AboutAppActivity
import com.aait.wasite.UI.Activities.AppInfo.CallUsActivity
import com.aait.wasite.UI.Activities.AppInfo.ComplainActivity
import com.aait.wasite.UI.Activities.AppInfo.SettingsActivity
import com.aait.wasite.UI.Activities.Auth.LoginActivity
import com.aait.wasite.UI.Activities.Auth.MyDataActivity
import com.aait.wasite.UI.Activities.SplashActivity
import com.aait.wasite.UI.Controllers.CatsAdapter
import com.aait.wasite.UI.Controllers.SliderAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.DialogUtil
import com.aait.wasite.Utils.PermissionUtils

import com.github.islamkhsh.CardSliderViewPager
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class MainActivity:ParentActivity(),OnItemClickListener,
    GpsTrakerListener {
    override fun onItemClick(view: View, position: Int) {
        if (catsModels.get(position).key.equals("services")){
            val intent = Intent(this,TransportActivity::class.java)
            intent.putExtra("item",catsModels.get(position))
            startActivity(intent)
        }else{
            if(user.loginStatus!!) {
                val intent = Intent(this, SubCatsActivity::class.java)
                intent.putExtra("item", catsModels.get(position))
                startActivity(intent)
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var menu: ImageView
    lateinit var cats:RecyclerView
    lateinit var viewpager: CardSliderViewPager
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var catsAdapter: CatsAdapter
    var catsModels = ArrayList<CategoryModel>()

    lateinit var about_app:LinearLayout
    lateinit var call_us:LinearLayout
    lateinit var complains:LinearLayout
    lateinit var profile:LinearLayout
    lateinit var main:LinearLayout
    lateinit var logout:LinearLayout
    lateinit var text:TextView
    lateinit var settings:LinearLayout
    lateinit var my_ads:LinearLayout
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""
    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        cats = findViewById(R.id.cats)
        text = findViewById(R.id.text)
        viewpager = findViewById(R.id.viewPager)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
//        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
//        linearLayoutManager.reverseLayout = false
//        linearLayoutManager.stackFromEnd = false
        catsAdapter = CatsAdapter(mContext,catsModels,R.layout.recycler_home)
        catsAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = catsAdapter
        sideMenu()
        getData()
        if(user.loginStatus!!){
            text.text = getString(R.string.logout)
        }else{
            text.text = getString(R.string.sign_in)
        }


    }

    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 80f)
        drawer_layout.setRadius(Gravity.END, 80f)
        drawer_layout.setViewScale(Gravity.START, 0.99f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.99f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 10f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 10f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(1f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

//        name = drawer_layout.findViewById(R.id.name)
//        image = drawer_layout.findViewById(R.id.image)
       main = drawer_layout.findViewById(R.id.main)
        profile = drawer_layout.findViewById(R.id.profile)
        settings = drawer_layout.findViewById(R.id.settings)
        my_ads = drawer_layout.findViewById(R.id.my_ads)
//        services = drawer_layout.findViewById(R.id.services)
        about_app = drawer_layout.findViewById(R.id.about_app)
        call_us = drawer_layout.findViewById(R.id.call_us)
        complains = drawer_layout.findViewById(R.id.complains)
//        settings = drawer_layout.findViewById(R.id.settings)
//        share_app = drawer_layout.findViewById(R.id.share_app)
        logout = drawer_layout.findViewById(R.id.logout)
//        name.text = mSharedPrefManager.userData.name!!
//        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        if (user.loginStatus!!){
            if (user.userData.driver==1){
                settings.visibility = View.VISIBLE
            }else{
                settings.visibility = View.GONE
            }
        }else{
            settings.visibility = View.GONE
        }
        profile.setOnClickListener {
            if(user.loginStatus!!) {
                startActivity(Intent(this, MyDataActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
                startActivity(Intent(this, LoginActivity::class.java))
            }}

        my_ads.setOnClickListener {
            if(user.loginStatus!!) {
                startActivity(Intent(this, MyAdsActivity::class.java))
            }else{
                CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
                startActivity(Intent(this, LoginActivity::class.java))
            }}
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity, AboutAppActivity::class.java)) }
        call_us.setOnClickListener { startActivity(Intent(this@MainActivity, CallUsActivity::class.java)) }
//        share_app.setOnClickListener { CommonUtil.ShareApp(mContext) }
        complains.setOnClickListener { startActivity(Intent(this@MainActivity, ComplainActivity::class.java)) }
       settings.setOnClickListener { getLocationWithPermission() }
//        delegates.setOnClickListener { startActivity(Intent(this@MainActivity,DelegatesActivity::class.java))
//        }
//        services.setOnClickListener {
//            if (mSharedPrefManager.userData.category_key.equals("cars")){
//                startActivity(Intent(this,CarServicesActivity::class.java))
//            }else if (mSharedPrefManager.userData.category_key.equals("locations")) {
//                startActivity(Intent(this, ServicesActivity::class.java))
//            }else if (mSharedPrefManager.userData.category_key.equals("clothes")) {
//                startActivity(Intent(this, ClothesServicesActivity::class.java))
//            }
//        }
        main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java)) }
//        orders.setOnClickListener { startActivity(Intent(this@MainActivity,OrdersActivity::class.java)) }
        logout.setOnClickListener {if(user.loginStatus!!){ logout() }
        else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
            startActivity(Intent(this, LoginActivity::class.java))
        }}
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Home(lang.appLanguage)?.enqueue(object:
            Callback<HomeResponse>{
            override fun onFailure(call: Call<HomeResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<HomeResponse>, response: Response<HomeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        catsAdapter.updateAll(response.body()?.categories!!)
                        initSliderAds(response.body()?.sliders!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun initSliderAds(list:ArrayList<String>){
        if(list.isEmpty()){
            viewpager.visibility= View.GONE

        }
        else{
            viewpager.visibility= View.VISIBLE

            viewpager.adapter= SliderAdapter(mContext!!,list)

        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut("Bearer "+user.userData.token,user.userData.device_id!!,"android",lang.appLanguage)?.enqueue(object :
            Callback<TermsResponse> {
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<TermsResponse>,
                response: Response<TermsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.loginStatus=false
                        user.Logout()

                        CommonUtil.makeToast(mContext!!,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext!!,response.body()?.msg!!)

                    }
                }
            }
        })
    }


    private var mAlertDialog: AlertDialog? = null


    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
              //  putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
               // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

              //  googleMap.clear()
                updateLat()
               // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }

    fun updateLat(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.UpdateLocation("Bearer"+user.userData.token,lang.appLanguage,mLat,mLang,result)?.enqueue(object:Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()

            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}