package com.aait.wasite.UI.Activities.Auth

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Models.TermsResponse
import com.aait.wasite.Models.UserModel
import com.aait.wasite.Models.UserResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ActivationCodeActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_activation_code
    lateinit var code:EditText
    lateinit var confirm:Button
    lateinit var resend:TextView
    lateinit var userModel:UserModel

    override fun initializeComponents() {
        userModel = intent.getSerializableExtra("user") as UserModel
        code = findViewById(R.id.code)
        confirm = findViewById(R.id.confirm)
        resend = findViewById(R.id.resend)
        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(code,getString(R.string.activation_code))){
                return@setOnClickListener
            }else{
                Activate()
            }
        }
        resend.setOnClickListener { Resend() }



    }

    fun Activate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.CheckCode("Bearer "+userModel.token!!,code.text.toString(),lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        startActivity(Intent(this@ActivationCodeActivity, LoginActivity::class.java))
                        this@ActivationCodeActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun Resend(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Resend("Bearer "+userModel.token!!,lang.appLanguage)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}