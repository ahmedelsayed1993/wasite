package com.aait.wasite.UI.Activities.Auth

import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.LocationActivity
import com.aait.wasite.UI.Activities.Main.MainActivity
import com.aait.wasite.UI.Controllers.ListAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import android.widget.ListAdapter as ListAdapter1

class DriverActivity:ParentActivity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.name) {
//            if (selected == 0) {
//                vehicleModel = vehicleModels.get(position)
//                vehicle.text = vehicleModel.name
//                vehicel_lay.visibility = View.GONE
//            } else {
                transportModel = transportModels.get(position)
                transport.text = transportModel.name
                transport_lay.visibility = View.GONE
           // }
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_driver
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var send:Button
    lateinit var vehicel_lay:LinearLayout
    lateinit var vehicals:RecyclerView
    lateinit var transport_lay:LinearLayout
    lateinit var transports:RecyclerView
   // lateinit var vehicle:TextView
    lateinit var transport:TextView
    lateinit var location:TextView
    lateinit var phone:EditText
    lateinit var whats:EditText
    lateinit var driving:ImageView
    lateinit var plats:ImageView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var vehicleAdapter: ListAdapter
    lateinit var transportAdapter: ListAdapter
    var vehicleModels = ArrayList<ServiceModel>()
    var transportModels = ArrayList<ServiceModel>()
    lateinit var vehicleModel:ServiceModel
    lateinit var transportModel:ServiceModel
    var selected = 0
    var address = ""
    var lat = ""
    var lng = ""

    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    internal var returnValue1: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options1 = Options.init()
        .setRequestCode(200)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue1)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath1: String? = null

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        send = findViewById(R.id.send)
        vehicel_lay = findViewById(R.id.vehicel_lay)
        vehicals = findViewById(R.id.vehicals)
        transport_lay = findViewById(R.id.transport_lay)
        transports = findViewById(R.id.transports)
       // vehicle = findViewById(R.id.vehicle)
        transport = findViewById(R.id.transport)
        location = findViewById(R.id.location)
        phone = findViewById(R.id.phone)
        whats = findViewById(R.id.whats)
        driving = findViewById(R.id.driving)
        plats = findViewById(R.id.plats)
        back.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Request_to_join_as_a_driver)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        vehicleAdapter = ListAdapter(mContext,vehicleModels,R.layout.recycler_list)
        transportAdapter = ListAdapter(mContext,transportModels,R.layout.recycler_list)
        vehicleAdapter.setOnItemClickListener(this)
        transportAdapter.setOnItemClickListener(this)
        vehicals.layoutManager = linearLayoutManager
        transports.layoutManager = linearLayoutManager1
        vehicals.adapter = vehicleAdapter
        transports.adapter = transportAdapter
//        vehicle.setOnClickListener {
//            selected = 0
//            getVehicles()
//        }
        transport.setOnClickListener {
            selected = 1
            getTransport()
        }
        location.setOnClickListener { startActivityForResult(Intent(this, LocationActivity::class.java),1)  }

        driving.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        plats.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options1)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options1)
            }
        }

        send.setOnClickListener {
            if(
                    CommonUtil.checkTextError(transport,getString(R.string.enter_transport))||
                    CommonUtil.checkTextError(location,getString(R.string.choose_location))||
                    CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(whats,getString(R.string.enter_whats))||
                    CommonUtil.checkLength(whats,getString(R.string.phone_length),9)){
                return@setOnClickListener
            }else{
                if (ImageBasePath==null){
                    CommonUtil.makeToast(mContext,getString(R.string.driving_licences))
                }else{
                    if(ImageBasePath1 == null){
                        CommonUtil.makeToast(mContext,getString(R.string.vehicle_plate))
                    }else{

                        JoinDriver(ImageBasePath!!,ImageBasePath1!!)
                    }
                }
            }
        }
    }


    fun getTransport(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Transport(lang.appLanguage)?.enqueue(object:
            Callback<ServicesResponse>{
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                transport_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        transportAdapter.updateAll(response.body()?.data!!)
                        transport_lay.visibility = View.VISIBLE
                    }else{
                        transport_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getVehicles(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Vehicle("Bearer "+user.userData.token!!,lang.appLanguage)?.enqueue(object:
            Callback<ServicesResponse>{
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                vehicel_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        vehicleAdapter.updateAll(response.body()?.data!!)
                        vehicel_lay.visibility = View.VISIBLE
                    }else{
                        vehicel_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                Glide.with(mContext).load(ImageBasePath).into(driving)

                if (ImageBasePath != null) {
                   // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else if (requestCode == 200) {
            if (resultCode==0){

            }else {
                returnValue1 = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath1 = returnValue1!![0]

                Glide.with(mContext).load(ImageBasePath1).into(plats)

                if (ImageBasePath1 != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }else {
            if (data?.getStringExtra("result") != null) {
                address = data?.getStringExtra("result").toString()
                lat = data?.getStringExtra("lat").toString()
                lng = data?.getStringExtra("lng").toString()
                location.text = address
            } else {
                address = ""
                lat = ""
                lng = ""
                location.text = ""
            }
        }

    }

    fun JoinDriver(path:String,path1:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("driving_license", ImageFile.name, fileBody)
        var filePart1: MultipartBody.Part? = null
        val ImageFile1 = File(path1)
        val fileBody1 = RequestBody.create(MediaType.parse("*/*"), ImageFile1)
        filePart1 = MultipartBody.Part.createFormData("vehicle_plate", ImageFile1.name, fileBody1)
        Client.getClient()?.create(Service::class.java)?.Join("Bearer "+user.userData.token!!,lang.appLanguage,null,transportModel.id!!,lat,lng,address,phone.text.toString()
        ,whats.text.toString(),filePart,filePart1)?.enqueue(object:Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        user.userData = response.body()?.data!!
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        startActivity(Intent(this@DriverActivity, MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}