package com.aait.wasite.UI.Activities.Main

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.ProductModel
import com.aait.wasite.Models.ProductsResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.ProductAdapter
import com.aait.wasite.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyAdsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_my_ads
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var productAdapter: ProductAdapter
    var productModels = ArrayList<ProductModel>()
    var productModels1 = ArrayList<ProductModel>()
    lateinit var text: EditText
    lateinit var search:ImageView

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        text = findViewById(R.id.text)
        search = findViewById(R.id.search)
        title.text = getString(R.string.my_ads)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext,productModels,R.layout.recycle_products)
        productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = productAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

        search.setOnClickListener {
            productModels1.clear()
            for (i in 0.. productModels.size-1){
                if (productModels.get(i).title!!.contains(text.text.toString())){
                    productModels1.add(productModels.get(i))
                }
            }
            //hideKeyboard()
            if (productModels1.isEmpty()!!) {
                layNoItem!!.visibility = View.VISIBLE
                layNoInternet!!.visibility = View.GONE
                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


            } else {
                productAdapter.updateAll(productModels1)
            }
            Log.e("ttt", Gson().toJson(productModels1))
            if (text.text.toString().equals("")){
                getData()
            }
        }

    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.MyProducts("Bearer "+user.userData.token!!,lang.appLanguage)?.enqueue(object:
            Callback<ProductsResponse> {
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
                            Log.e("products",Gson().toJson(response.body()!!.data))
//
                            productAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProductActivity::class.java)

        intent.putExtra("item",productModels.get(position))
        startActivity(intent)
    }
}