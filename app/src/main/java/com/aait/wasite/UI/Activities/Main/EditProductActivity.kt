package com.aait.wasite.UI.Activities.Main

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.DeleteImageAdapter
import com.aait.wasite.UI.Controllers.ImageAdapter
import com.aait.wasite.UI.Controllers.ListAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class EditProductActivity : ParentActivity(), OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_edit_product

    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var send: Button
    lateinit var type_lay: LinearLayout
    lateinit var types: RecyclerView
    lateinit var product_ar: EditText
   // lateinit var product_en: EditText
    lateinit var category: TextView
    lateinit var description_ar: EditText
    //lateinit var description_en: EditText
    lateinit var phone: EditText
    lateinit var whats: EditText
    lateinit var add_image: ImageView
    lateinit var images: RecyclerView
    lateinit var listAdapter: ListAdapter
    lateinit var typeModel: ServiceModel
    var typeModels = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    var imgs = ArrayList<ImageModel>()
    lateinit var city_lay:LinearLayout
    lateinit var cities:RecyclerView
    lateinit var listAdapter1: ListAdapter
    lateinit var CityModel: ServiceModel
    var cityModels = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager3: LinearLayoutManager
    lateinit var city:TextView
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(6)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    private var paths = ArrayList<String>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var linearLayoutManager2: LinearLayoutManager
    lateinit var imageAdapter: DeleteImageAdapter
    lateinit var imagesAdapter: ImageAdapter
    lateinit var image:RecyclerView
    var id = 0
    var cat = 0
     var ids=ArrayList<Int>()
    var selected = 0
    override fun initializeComponents() {
        ids.clear()
        id = intent.getIntExtra("id",0)
        hideKeyboard()
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        send = findViewById(R.id.send)
        type_lay = findViewById(R.id.type_lay)
        types = findViewById(R.id.types)
        product_ar = findViewById(R.id.product_ar)
       // product_en = findViewById(R.id.product_en)
        category = findViewById(R.id.category)
        description_ar = findViewById(R.id.description_ar)
        //description_en = findViewById(R.id.description_en)
        phone = findViewById(R.id.phone)
        whats = findViewById(R.id.whats)
        image = findViewById(R.id.image)
        city_lay = findViewById(R.id.city_lay)
        cities = findViewById(R.id.cities)
        city = findViewById(R.id.city)
        title.text = getString(R.string.edit_product)
        back.setOnClickListener { onBackPressed()
            finish()}
        add_image = findViewById(R.id.add_image)
        images = findViewById(R.id.images)
        listAdapter = ListAdapter(mContext,typeModels, R.layout.recycler_list)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        listAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = listAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,false)
        imageAdapter = DeleteImageAdapter(mContext,imgs, R.layout.recycler_img)
        imageAdapter.setOnItemClickListener(this)
        images.layoutManager = linearLayoutManager1
        images.adapter = imageAdapter
        linearLayoutManager2 = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL,false)
        imagesAdapter = ImageAdapter(mContext,ArrayList<String>(), R.layout.recycler_image)
       // imageAdapter.setOnItemClickListener(this)
        image.layoutManager = linearLayoutManager2
        image.adapter = imagesAdapter
        listAdapter1 = ListAdapter(mContext,cityModels,R.layout.recycler_list)
        linearLayoutManager3 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1.setOnItemClickListener(this)
        cities.layoutManager = linearLayoutManager3
        cities.adapter = listAdapter1
        category.setOnClickListener {
            selected = 0
            getTypes() }
        city.setOnClickListener {
            selected = 1
            getCities() }
        add_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        getProduct()



        send.setOnClickListener {
            if (
                CommonUtil.checkEditError(product_ar,getString(R.string.enter_product_name))||
                // CommonUtil.checkEditError(product_en,getString(R.string.enter_product_name))||
                CommonUtil.checkTextError(category,getString(R.string.choose_main_category))||
                CommonUtil.checkTextError(city,getString(R.string.choose_city))||
                CommonUtil.checkEditError(description_ar,getString(R.string.enter_product_description))||
                // CommonUtil.checkEditError(description_en,getString(R.string.enter_product_description))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)
//                ||
//                CommonUtil.checkEditError(whats,getString(R.string.enter_whats))||
//                CommonUtil.checkLength(whats,getString(R.string.phone_length),9)
            ){
                return@setOnClickListener
            }else{
                if(paths.isEmpty()){
                    AddProduct()
                }else{
                    AddProduct(paths)
                }
            }
        }

    }
    fun getProduct(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditPro("Bearer "+user.userData.token!!,lang.appLanguage,id
        ,null,null,null,null,null,null,null,null,null)?.enqueue(object:Callback<EditProductResponse>{
            override fun onFailure(call: Call<EditProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<EditProductResponse>,
                response: Response<EditProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        product_ar.setText(response.body()?.data?.title_ar)
                      //  product_en.setText(response.body()?.data?.title_en)
                        description_ar.setText(response.body()?.data?.description_ar)
                      //  description_en.setText(response.body()?.data?.description_en)
                        phone.setText(response.body()?.data?.phone)
                        whats.setText(response.body()?.data?.whatsapp)
                        typeModel = ServiceModel(response.body()?.data?.subcategory_id,response.body()?.data?.subcategory_name)
                        CityModel = ServiceModel(response.body()?.data?.city_id,response.body()?.data?.city_name)
                        city.text = CityModel.name
                        category.text = typeModel.name
                        cat = response.body()?.data?.category_id!!
                        imageAdapter.updateAll(response.body()?.data?.images!!)
                        if (response.body()?.data?.images!!.isEmpty()){
                            images.visibility = View.GONE
                        }else{
                            images.visibility = View.VISIBLE
                        }
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun AddProduct(imges:ArrayList<String>){
        var imgs = ArrayList<MultipartBody.Part>()
        for (i in 0..imges.size-1){
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(imges.get(i))
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
            imgs.add(filePart)
        }
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditProduct("Bearer "+user.userData.token!!,lang.appLanguage,id,typeModel.id!!,CityModel.id!!
            ,product_ar.text.toString(),null,description_ar.text.toString(),null,phone.text.toString()
            ,whats.text.toString(),ids.joinToString(",","",""),imgs)?.enqueue(object: Callback<EditProductResponse> {
            override fun onFailure(call: Call<EditProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<EditProductResponse>,
                response: Response<EditProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                        finish()
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun AddProduct(){

        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.EditPro("Bearer "+user.userData.token!!,lang.appLanguage,id,typeModel.id!!,CityModel.id!!
            ,product_ar.text.toString(),null,description_ar.text.toString(),null,phone.text.toString()
            ,whats.text.toString(),ids.joinToString(",","",""))?.enqueue(object: Callback<EditProductResponse> {
            override fun onFailure(call: Call<EditProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<EditProductResponse>,
                response: Response<EditProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        onBackPressed()
                        finish()
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.delete){
            ids.add(imgs.get(position).id!!)
            imgs.remove(imgs.get(position))
            imageAdapter.notifyDataSetChanged()

            Log.e("ids",ids.joinToString(",","",""))
        }else {
            if (selected == 0) {
                typeModel = typeModels.get(position)
                category.text = typeModel.name
                type_lay.visibility = View.GONE
            }else{
                CityModel = cityModels.get(position)
                city.text = CityModel.name
                city_lay.visibility = View.GONE
            }
        }
    }

    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCat("Bearer "+user.userData.token!!,lang.appLanguage,cat)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                type_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                        type_lay.visibility = View.VISIBLE
                    }else{
                        type_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
                for (i in 0..returnValue!!.size-1){
                    paths.add(returnValue!![i])
                }
                if(paths.size==1){

                    image.visibility = View.GONE
                }else{

                    image.visibility = View.VISIBLE
                    imagesAdapter.updateAll(paths)
                }
                Log.e("pathsss", Gson().toJson(returnValue))



                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                city_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter1.updateAll(response.body()?.data!!)
                        city_lay.visibility = View.VISIBLE
                    }else{
                        city_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
}