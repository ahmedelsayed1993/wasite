package com.aait.wasite.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.wasite.Base.ParentRecyclerAdapter
import com.aait.wasite.Base.ParentRecyclerViewHolder
import com.aait.wasite.Models.ServiceModel
import com.aait.wasite.R
import com.bumptech.glide.Glide

class ListAdapter  (context: Context, data: MutableList<ServiceModel>, layoutId: Int) :
    ParentRecyclerAdapter<ServiceModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val serviceModel = data.get(position)
        viewHolder.name!!.setText(serviceModel.name)

        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<TextView>(R.id.name)

    }
}