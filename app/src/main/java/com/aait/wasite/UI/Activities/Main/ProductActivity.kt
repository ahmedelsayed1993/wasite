package com.aait.wasite.UI.Activities.Main

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.CommentsAdapter
import com.aait.wasite.UI.Controllers.ImagesAdapter
import com.aait.wasite.UI.Controllers.SliderAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductActivity : ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_product

    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var images: RecyclerView
    lateinit var imagesAdapter: ImagesAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var name: TextView
    lateinit var username: TextView
    lateinit var description: TextView
    lateinit var phone: LinearLayout
    lateinit var number: TextView
    lateinit var whats: LinearLayout
    lateinit var whatsapp: TextView

    lateinit var item: ProductModel
    lateinit var edit:Button
    lateinit var delete:Button
    var list = ArrayList<String>()
    lateinit var commentsAdapter: CommentsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var commentModels = ArrayList<CommentModel>()
    lateinit var comments:RecyclerView
    override fun initializeComponents() {

        item = intent.getSerializableExtra("item") as ProductModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        images = findViewById(R.id.images)
        imagesAdapter = ImagesAdapter(mContext,list,R.layout.recycle_image)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        imagesAdapter.setOnItemClickListener(this)
        images.layoutManager = linearLayoutManager
        images.adapter = imagesAdapter
        name = findViewById(R.id.name)
        comments = findViewById(R.id.comments)
        username = findViewById(R.id.user)
        description = findViewById(R.id.description)
        phone = findViewById(R.id.phone)
        number = findViewById(R.id.number)
        whats = findViewById(R.id.whats)
        whatsapp = findViewById(R.id.whatsapp)
        edit = findViewById(R.id.edit)
        delete = findViewById(R.id.delete)
        title.text = item.title
        back.setOnClickListener { onBackPressed()
            finish()}
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        commentsAdapter = CommentsAdapter(mContext,commentModels,R.layout.recycle_comment)
        comments.layoutManager = linearLayoutManager1
        comments.adapter = commentsAdapter
        getData()
        edit.setOnClickListener { val intent = Intent(this,EditProductActivity::class.java)
        intent.putExtra("id",item.id)
        startActivity(intent)
        }
        delete.setOnClickListener {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.Delete("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!)?.enqueue(
                object: Callback<TermsResponse> {
                    override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext,t)
                        t.printStackTrace()
                        hideProgressDialog()
                    }

                    override fun onResponse(
                        call: Call<TermsResponse>,
                        response: Response<TermsResponse>
                    ) {
                        hideProgressDialog()
                        if(response.isSuccessful){
                            if (response.body()?.value.equals("1")){
                                CommonUtil.makeToast(mContext,response.body()?.data!!)
                               startActivity(Intent(this@ProductActivity,MainActivity::class.java))
                                finish()
                            }else{
                                CommonUtil.makeToast(mContext,response.body()?.msg!!)
                            }
                        }
                    }

                }
            )

        }
        phone.setOnClickListener {
            if (number.text.equals("")){

            }else{
                getLocationWithPermission(number.text.toString())
            }
        }
        whats.setOnClickListener {
            if (whatsapp.text.equals("")){

            }else{
                openWhatsAppConversationUsingUri(mContext!!,whatsapp.text.toString().replaceFirst("0","+966",false),"")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.GetProduct("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!)?.enqueue(
            object: Callback<ProductDetailsResponse> {
                override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ProductDetailsResponse>,
                    response: Response<ProductDetailsResponse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            imagesAdapter.updateAll(response.body()?.data?.images!!)
                            commentsAdapter.updateAll(response.body()?.data?.comments!!)
                            name.text = response.body()?.data?.title
                            username.text = response.body()?.data?.username
                            description.text = response.body()?.data?.description
                            number.text = response.body()?.data?.phone
                            whatsapp.text = response.body()?.data?.whatsapp
                            if (response.body()?.data?.whatsapp.equals("")){
                                whats.visibility = View.GONE
                            }else{
                                whats.visibility = View.VISIBLE
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.GetProduct("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!)?.enqueue(
            object: Callback<ProductDetailsResponse> {
                override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ProductDetailsResponse>,
                    response: Response<ProductDetailsResponse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            imagesAdapter.updateAll(response.body()?.data?.images!!)
                            commentsAdapter.updateAll(response.body()?.data?.comments!!)
                            name.text = response.body()?.data?.title
                            username.text = response.body()?.data?.username
                            description.text = response.body()?.data?.description
                            number.text = response.body()?.data?.phone
                            whatsapp.text = response.body()?.data?.whatsapp
                            if (response.body()?.data?.whatsapp.equals("")){
                                whats.visibility = View.GONE
                            }else{
                                whats.visibility = View.VISIBLE
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )



    }

//    fun initSliderAds(list:ArrayList<String>){
//        if(list.isEmpty()){
//            viewPager.visibility= View.GONE
//            indicator.visibility = View.GONE
//
//        }
//        else{
//            viewPager.visibility= View.VISIBLE
//            indicator.visibility = View.VISIBLE
//            viewPager.adapter= SliderAdapter(mContext!!,list)
//            indicator.setViewPager(viewPager)
//        }
//    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }

    override fun onItemClick(view: View, position: Int) {
        val intent  = Intent(this, ImagesActivity::class.java)
        intent.putExtra("link",list)
        startActivity(intent)
    }
}