package com.aait.wasite.UI.Activities.Main

import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.AddProductResponse
import com.aait.wasite.Models.CategoryModel
import com.aait.wasite.Models.ServiceModel
import com.aait.wasite.Models.ServicesResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.ImageAdapter
import com.aait.wasite.UI.Controllers.ListAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class AddProductActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_add_product

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var send:Button
    lateinit var type_lay:LinearLayout
    lateinit var types:RecyclerView
    lateinit var city_lay:LinearLayout
    lateinit var cities:RecyclerView
    lateinit var product_ar:EditText
   // lateinit var product_en:EditText
    lateinit var category:TextView
    lateinit var city:TextView
    lateinit var description_ar:EditText
   // lateinit var description_en:EditText
    lateinit var phone:EditText
    lateinit var whats:EditText
    lateinit var image:ImageView
    lateinit var add_image:ImageView
    lateinit var images:RecyclerView
    lateinit var cat :CategoryModel
    lateinit var listAdapter: ListAdapter
    lateinit var typeModel: ServiceModel
    var typeModels = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var listAdapter1: ListAdapter
    lateinit var CityModel: ServiceModel
    var cityModels = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager2: LinearLayoutManager
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(6)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null
    private var paths = ArrayList<String>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var imageAdapter:ImageAdapter
    var selected = 0
    override fun initializeComponents() {
        cat = intent.getSerializableExtra("cat") as CategoryModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        send = findViewById(R.id.send)
        type_lay = findViewById(R.id.type_lay)
        types = findViewById(R.id.types)
        city_lay = findViewById(R.id.city_lay)
        cities = findViewById(R.id.cities)
        product_ar = findViewById(R.id.product_ar)
       // product_en = findViewById(R.id.product_en)
        category = findViewById(R.id.category)
        city = findViewById(R.id.city)
        description_ar = findViewById(R.id.description_ar)
       // description_en = findViewById(R.id.description_en)
        phone = findViewById(R.id.phone)
        whats = findViewById(R.id.whats)
        image = findViewById(R.id.image)
        title.text = getString(R.string.add_new_product)
        back.setOnClickListener { onBackPressed()
        finish()}
        hideKeyboard()
        add_image = findViewById(R.id.add_image)
        images = findViewById(R.id.images)
        listAdapter = ListAdapter(mContext,typeModels,R.layout.recycler_list)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = listAdapter
        listAdapter1 = ListAdapter(mContext,cityModels,R.layout.recycler_list)
        linearLayoutManager2 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter1.setOnItemClickListener(this)
        cities.layoutManager = linearLayoutManager2
        cities.adapter = listAdapter1
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        imageAdapter = ImageAdapter(mContext,ArrayList<String>(),R.layout.recycler_image)
        images.layoutManager = linearLayoutManager1
        images.adapter = imageAdapter
        category.setOnClickListener {
            selected = 0
            getTypes() }

        city.setOnClickListener {
            selected = 1
            getCities() }
        phone.setText(user.userData.phone)
        add_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        send.setOnClickListener {
            if (
                CommonUtil.checkEditError(product_ar,getString(R.string.enter_product_name))||
                   // CommonUtil.checkEditError(product_en,getString(R.string.enter_product_name))||
                    CommonUtil.checkTextError(category,getString(R.string.choose_main_category))||
                CommonUtil.checkTextError(city,getString(R.string.choose_city))||
                    CommonUtil.checkEditError(description_ar,getString(R.string.enter_product_description))||
                   // CommonUtil.checkEditError(description_en,getString(R.string.enter_product_description))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)
//                ||
//                CommonUtil.checkEditError(whats,getString(R.string.enter_whats))||
//                CommonUtil.checkLength(whats,getString(R.string.phone_length),9)
            ){
                return@setOnClickListener
            }else{
                if(paths.isEmpty()){
                    AddProduct()
                }else{
                       AddProduct(paths)
                }
            }
        }

    }

    fun AddProduct(imges:ArrayList<String>){
        var imgs = ArrayList<MultipartBody.Part>()
        for (i in 0..imges.size-1){
            var filePart: MultipartBody.Part? = null
            val ImageFile = File(imges.get(i))
            val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.name, fileBody)
            imgs.add(filePart)
        }
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddProduct("Bearer "+user.userData.token!!,lang.appLanguage,typeModel.id!!
        ,CityModel.id!!,product_ar.text.toString(),null,description_ar.text.toString(),null,phone.text.toString()
        ,whats.text.toString(),imgs)?.enqueue(object:Callback<AddProductResponse>{
            override fun onFailure(call: Call<AddProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AddProductResponse>,
                response: Response<AddProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@AddProductActivity,ProductDetailsActivity::class.java)
                        intent.putExtra("id",response.body()?.data!!)
                        intent.putExtra("item",cat)
                        startActivity(intent)
                        finish()
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun AddProduct(){

        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddProductwithout("Bearer "+user.userData.token!!,lang.appLanguage,typeModel.id!!,CityModel.id!!
            ,product_ar.text.toString(),null,description_ar.text.toString(),null,phone.text.toString()
            ,whats.text.toString())?.enqueue(object:Callback<AddProductResponse>{
            override fun onFailure(call: Call<AddProductResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AddProductResponse>,
                response: Response<AddProductResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@AddProductActivity,ProductDetailsActivity::class.java)
                        intent.putExtra("id",response.body()?.data!!)
                        intent.putExtra("item",cat)
                        startActivity(intent)
                        finish()
                    }
                    else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onItemClick(view: View, position: Int) {
        if (selected == 0) {
            typeModel = typeModels.get(position)
            category.text = typeModel.name
            type_lay.visibility = View.GONE
        }else{
            CityModel = cityModels.get(position)
            city.text = CityModel.name
            city_lay.visibility = View.GONE
        }
    }

    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCat("Bearer "+user.userData.token!!,lang.appLanguage,cat.id!!)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                type_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                        type_lay.visibility = View.VISIBLE
                    }else{
                        type_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
    fun getCities(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Cities(lang.appLanguage)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                city_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter1.updateAll(response.body()?.data!!)
                        city_lay.visibility = View.VISIBLE
                    }else{
                        city_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == 0) {

            } else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]
                for (i in 0..returnValue!!.size-1){
                    paths.add(returnValue!![i])
                }
                if(paths.size==1){
                    image.visibility = View.VISIBLE
                    images.visibility = View.GONE
                }else{
                    image.visibility = View.GONE
                    images.visibility = View.VISIBLE
                    imageAdapter.updateAll(paths)
                }
                Log.e("pathsss", Gson().toJson(paths))

                Glide.with(mContext).load(ImageBasePath).into(image)

                if (ImageBasePath != null) {
                    // upLoad(ImageBasePath!!)
                    //ID.text = getString(R.string.Image_attached)
                }
            }
        }
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
}