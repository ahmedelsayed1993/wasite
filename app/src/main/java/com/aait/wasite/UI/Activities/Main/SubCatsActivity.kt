package com.aait.wasite.UI.Activities.Main

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.ProductAdapter
import com.aait.wasite.UI.Controllers.SubCatsAdapter
import com.aait.wasite.Utils.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SubCatsActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_subcats

    lateinit var item: CategoryModel
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cats:RecyclerView
    lateinit var subCatsAdapter: SubCatsAdapter
    var subs = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    lateinit var productAdapter: ProductAdapter
    var productModels = ArrayList<ProductModel>()
    var productModels1 = ArrayList<ProductModel>()
    lateinit var add:ImageView
    lateinit var text: EditText
    lateinit var search:ImageView
    override fun initializeComponents() {
        item = intent.getSerializableExtra("item") as CategoryModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        add = findViewById(R.id.add)
        back.setOnClickListener { onBackPressed()
            finish()}
        text = findViewById(R.id.text)
        search = findViewById(R.id.search)
        title.text = item.name
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false)
        cats = findViewById(R.id.cats)
        subCatsAdapter = SubCatsAdapter(mContext, subs as MutableList<ServiceModel>,R.layout.recycler_sub_cat)
        subCatsAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = subCatsAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        productAdapter = ProductAdapter(mContext,productModels,R.layout.recycler_products)
        productAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager1
        rv_recycle.adapter = productAdapter
        add.setOnClickListener {
            val intent = Intent(this,AddProductActivity::class.java)
            intent.putExtra("cat",item)
            startActivity(intent) }
        getSubs()
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData(null)

        }

        getData(null)
        text.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                productModels1.clear()
                for (i in 0.. productModels.size-1){
                    if (productModels.get(i).title!!.contains(text.text.toString())){
                        productModels1.add(productModels.get(i))
                    }
                }
                //hideKeyboard()
                if (productModels1.isEmpty()!!) {
                    layNoItem!!.visibility = View.VISIBLE
                    layNoInternet!!.visibility = View.GONE
                    tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


                } else {
                    productAdapter.updateAll(productModels1)
                }
                Log.e("ttt", Gson().toJson(productModels1))
                if (text.text.toString().equals("")){
                    getData(null)
                }
            }

        })
        search.setOnClickListener {
            productModels1.clear()
            for (i in 0.. productModels.size-1){
                if (productModels.get(i).title!!.contains(text.text.toString())){
                    productModels1.add(productModels.get(i))
                }
            }
            //hideKeyboard()
            if (productModels1.isEmpty()!!) {
                layNoItem!!.visibility = View.VISIBLE
                layNoInternet!!.visibility = View.GONE
                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)


            } else {
                productAdapter.updateAll(productModels1)
            }
            Log.e("ttt", Gson().toJson(productModels1))
            if (text.text.toString().equals("")){
                getData(null)
            }
        }
    }

    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name){
            subCatsAdapter.selected = position
            subs.get(position).selected = true
            subCatsAdapter.notifyDataSetChanged()
            if(subs.get(position).id==0){
                getData(null)
            }else{
                getData(subs.get(position).id!!)
            }

        }else{
            val intent = Intent(this,ProductDetailsActivity::class.java)
            intent.putExtra("id",productModels.get(position).id)
            intent.putExtra("item",item)
            startActivity(intent)
        }

    }

    fun getSubs(){
        showProgressDialog(getString(R.string.please_wait))
        subs.clear()
        Client.getClient()?.create(Service::class.java)?.SubCat("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!)
            ?.enqueue(object: Callback<ServicesResponse>{
                override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ServicesResponse>,
                    response: Response<ServicesResponse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if(response.body()?.value.equals("1")){
                            subs = response.body()?.data!!
                            subs.add(0,ServiceModel(0,getString(R.string.all)))
                            subCatsAdapter.updateAll(subs)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }

    fun getData(id:Int?){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Products("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!,id)?.enqueue(object:
            Callback<ProductsResponse> {
            override fun onFailure(call: Call<ProductsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ProductsResponse>,
                response: Response<ProductsResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            productAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}