package com.aait.wasite.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.aait.wasite.Base.ParentRecyclerAdapter
import com.aait.wasite.Base.ParentRecyclerViewHolder
import com.aait.wasite.Models.CategoryModel
import com.aait.wasite.Models.ServiceModel
import com.aait.wasite.R
import com.bumptech.glide.Glide

class TransportAdapter (context: Context, data: MutableList<ServiceModel>, layoutId: Int) :
    ParentRecyclerAdapter<ServiceModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.name)
        Glide.with(mcontext).load(questionModel.image).into(viewHolder.image)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)




    }
}