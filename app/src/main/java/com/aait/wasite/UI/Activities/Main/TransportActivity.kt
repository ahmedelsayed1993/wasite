package com.aait.wasite.UI.Activities.Main

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.CategoryModel
import com.aait.wasite.Models.ServiceModel
import com.aait.wasite.Models.ServicesResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.Auth.LoginActivity
import com.aait.wasite.UI.Controllers.TransportAdapter
import com.aait.wasite.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransportActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_transport
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    lateinit var gridLayoutManager: LinearLayoutManager
    lateinit var transportAdapter: TransportAdapter
    var transportModels = ArrayList<ServiceModel>()
    lateinit var item:CategoryModel

    override fun initializeComponents() {
        item = intent.getSerializableExtra("item") as CategoryModel
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = item.name
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        gridLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        transportAdapter = TransportAdapter(mContext,transportModels,R.layout.recycler_transport)
        transportAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = gridLayoutManager
        rv_recycle.adapter = transportAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()
    }

    override fun onItemClick(view: View, position: Int) {
        if(user.loginStatus!!) {
            val intent = Intent(this, RequestActivity::class.java)
            intent.putExtra("item", transportModels.get(position))
            startActivity(intent)

        }else{
            CommonUtil.makeToast(mContext,getString(R.string.you_visitor))
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Transport(lang.appLanguage)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                swipeRefresh!!.isRefreshing = false
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()!!.data?.isEmpty()!!) {
                            layNoItem!!.visibility = View.VISIBLE
                            layNoInternet!!.visibility = View.GONE
                            tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                        } else {
//
                            transportAdapter.updateAll(response.body()!!.data!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}