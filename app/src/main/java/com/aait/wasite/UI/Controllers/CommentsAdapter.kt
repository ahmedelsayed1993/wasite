package com.aait.wasite.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import com.aait.wasite.Base.ParentRecyclerAdapter
import com.aait.wasite.Base.ParentRecyclerViewHolder
import com.aait.wasite.Models.CommentModel
import com.aait.wasite.Models.DriverModel
import com.aait.wasite.R
import com.bumptech.glide.Glide

class CommentsAdapter (context: Context, data: MutableList<CommentModel>, layoutId: Int) :
    ParentRecyclerAdapter<CommentModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.username)
        viewHolder.time!!.setText(questionModel.created)
        viewHolder.comment!!.setText(questionModel.comment)
        viewHolder.rating.rating = questionModel.rate!!
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)





    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var time = itemView.findViewById<TextView>(R.id.time)
        internal var comment = itemView.findViewById<TextView>(R.id.comment)
        internal var rating = itemView.findViewById<RatingBar>(R.id.rating)




    }
}