package com.aait.wasite.UI.Activities.Main

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.CommentsAdapter
import com.aait.wasite.UI.Controllers.ImagesAdapter
import com.aait.wasite.UI.Controllers.SliderAdapter
import com.aait.wasite.Utils.CommonUtil
import com.aait.wasite.Utils.PermissionUtils
import com.github.islamkhsh.CardSliderViewPager
import com.google.firebase.dynamiclinks.DynamicLink
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductDetailsActivity:ParentActivity() ,OnItemClickListener{
    override val layoutResource: Int
        get() = R.layout.activity_product_details

    lateinit var title:TextView
    lateinit var back:ImageView

    lateinit var name:TextView
    lateinit var username:TextView
    lateinit var description:TextView
    lateinit var phone:LinearLayout
    lateinit var number:TextView
    lateinit var whats:LinearLayout
    lateinit var whatsapp:TextView
    lateinit var images:RecyclerView
    lateinit var imagesAdapter: ImagesAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var commentsAdapter: CommentsAdapter
    lateinit var linearLayoutManager1: LinearLayoutManager
    var commentModels = ArrayList<CommentModel>()
    var id = 0
    lateinit var item:CategoryModel
   var list = ArrayList<String>()
    lateinit var comment:TextView
    lateinit var share:ImageView
    lateinit var comments:RecyclerView
    var link = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        item = intent.getSerializableExtra("item") as CategoryModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
       images = findViewById(R.id.images)
        comment = findViewById(R.id.comment)
        comments = findViewById(R.id.comments)
        share = findViewById(R.id.share)
        imagesAdapter = ImagesAdapter(mContext,list,R.layout.recycle_image)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        imagesAdapter.setOnItemClickListener(this)
        images.layoutManager = linearLayoutManager
        images.adapter = imagesAdapter
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        commentsAdapter = CommentsAdapter(mContext,commentModels,R.layout.recycle_comment)
        comments.layoutManager = linearLayoutManager1
        comments.adapter = commentsAdapter
        name = findViewById(R.id.name)
        username = findViewById(R.id.user)
        description = findViewById(R.id.description)
        phone = findViewById(R.id.phone)
        number = findViewById(R.id.number)
        whats = findViewById(R.id.whats)
        whatsapp = findViewById(R.id.whatsapp)
        title.text = item.name
        back.setOnClickListener { onBackPressed()
        finish()}

        getData()
        share.setOnClickListener {
           if (link.equals("")){

           }else{
               Log.e("link",link)
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, link)

            startActivity(Intent.createChooser(intent, link))} }
        phone.setOnClickListener {


            if (number.text.equals("")){

            }else{
                getLocationWithPermission(number.text.toString())
            }
        }
        comment.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_rate)
            val old_pass = dialog?.findViewById<EditText>(R.id.coment)
            val rating = dialog?.findViewById<RatingBar>(R.id.rating)

            val save = dialog?.findViewById<Button>(R.id.confirm)


            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.add_comment))){
                    return@setOnClickListener
                }else {
                    showProgressDialog(getString(R.string.please_wait))
                    Client.getClient()?.create(Service::class.java)?.AddComment("Bearer"+user.userData.token,lang.appLanguage
                    ,id,old_pass.text.toString(),rating.rating.toInt())?.enqueue(object :Callback<TermsResponse>{
                        override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                            hideProgressDialog()
                            CommonUtil.handleException(mContext,t)
                            t.printStackTrace()
                            dialog?.dismiss()
                        }

                        override fun onResponse(
                            call: Call<TermsResponse>,
                            response: Response<TermsResponse>
                        ) {
                            hideProgressDialog()
                            dialog?.dismiss()
                            if (response.isSuccessful){
                                if (response.body()?.value.equals("1")){
                                    CommonUtil.makeToast(mContext,response.body()?.data!!)
                                    getData()

                                }else{
                                    CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                }
                            }
                        }

                    })
                }

            }
            dialog?.show()
        }
        whats.setOnClickListener {
            if (whatsapp.text.equals("")){

            }else{
                openWhatsAppConversationUsingUri(mContext!!,whatsapp.text.toString().replaceFirst("0","+966",false),"")
            }
        }
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.GetProduct("Bearer "+user.userData.token!!,lang.appLanguage,id)?.enqueue(
            object: Callback<ProductDetailsResponse>{
                override fun onFailure(call: Call<ProductDetailsResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<ProductDetailsResponse>,
                    response: Response<ProductDetailsResponse>
                ) {
                    hideProgressDialog()
                    if(response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            imagesAdapter.updateAll(response.body()?.data?.images!!)
                            name.text = response.body()?.data?.title
                            username.text = response.body()?.data?.username
                            description.text = response.body()?.data?.description
                            number.text = response.body()?.data?.phone
                            whatsapp.text = response.body()?.data?.whatsapp
                            link = response.body()?.data?.share_link!!
                            commentsAdapter.updateAll(response.body()?.data?.comments!!)
                            if (response.body()?.data?.whatsapp.equals("")){
                                whats.visibility = View.GONE
                            }else{
                                whats.visibility = View.VISIBLE
                            }
                        }
                    }
                }

            }
        )



    }

//    fun initSliderAds(list:ArrayList<String>){
//        if(list.isEmpty()){
//            viewPager.visibility= View.GONE
//            indicator.visibility = View.GONE
//
//        }
//        else{
//            viewPager.visibility= View.VISIBLE
//            indicator.visibility = View.VISIBLE
//            viewPager.adapter= SliderAdapter(mContext!!,list)
//            indicator.setViewPager(viewPager)
//        }
//    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }

    override fun onItemClick(view: View, position: Int) {
        val intent  = Intent(this, ImagesActivity::class.java)
        intent.putExtra("link",list)
        startActivity(intent)
    }

    fun generateContentLink(): Uri {
        val baseUrl = Uri.parse(".UI.Activities.Main.ProductDetailsActivity (Wasite.app)")
        val domain = "https://wasitalnaql.aait-sa.com"

        val link = FirebaseDynamicLinks.getInstance()
            .createDynamicLink()
            .setLink(baseUrl)
            .setDomainUriPrefix(domain)
           // .setIosParameters(DynamicLink.IosParameters.Builder("com.your.bundleid").build())
            .setAndroidParameters(DynamicLink.AndroidParameters.Builder("com.aait.wasite").build())
            .buildDynamicLink()

        Log.e("link",link.uri.toString())

        return link.uri
    }
}