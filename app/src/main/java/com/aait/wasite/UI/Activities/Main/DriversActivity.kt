package com.aait.wasite.UI.Activities.Main

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.DriverModel
import com.aait.wasite.R
import com.aait.wasite.UI.Controllers.DriverAdapter
import com.aait.wasite.Utils.PermissionUtils
import com.google.gson.Gson

class DriversActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_transport

    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    var driverModels = ArrayList<DriverModel>()
    var driverModels1 = ArrayList<DriverModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var driverAdapter:DriverAdapter

    override fun initializeComponents() {
        driverModels = intent.getSerializableExtra("driver") as ArrayList<DriverModel>
        Log.e("dataaa", Gson().toJson(driverModels))
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        back.setOnClickListener { onBackPressed()
            finish()}
        title.text = getString(R.string.choose_driver)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        driverAdapter = DriverAdapter(mContext,driverModels1,R.layout.recycler_driver)
        driverAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = driverAdapter

        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener {
            getData()

        }

        getData()

    }
    fun getData(){
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        swipeRefresh!!.isRefreshing = false

        Log.e("data", Gson().toJson(driverModels))
                if (driverModels.isEmpty()!!) {
                    layNoItem!!.visibility = View.VISIBLE
                    layNoInternet!!.visibility = View.GONE
                    tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)

                } else {
//
                    driverAdapter.updateAll(driverModels)
                }
    }

    override fun onItemClick(view: View, position: Int) {
        if(view.id == R.id.whats){
           // driverModels.get(position).whatsapp!!.replaceFirst("0","+966",false)
            openWhatsAppConversationUsingUri(mContext!!,driverModels.get(position).whatsapp!!.replaceFirst("0","+966",false),"")
        }else{
            getLocationWithPermission(driverModels.get(position).phone!!)
        }

    }

    internal fun callnumber(number: String) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$number")
        mContext!!.startActivity(intent)
    }

    fun getLocationWithPermission(number: String) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, Manifest.permission.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions(
                        mContext as Activity, PermissionUtils.CALL_PHONE,
                        300
                    )
            } else {
                callnumber(number)
            }
        } else {
            callnumber(number)
        }

    }

    fun openWhatsAppConversationUsingUri(
        context: Context,
        numberWithCountryCode: String,
        message: String
    ) {

        val uri =
            Uri.parse("https://api.whatsapp.com/send?phone=$numberWithCountryCode&text=$message")

        val sendIntent = Intent(Intent.ACTION_VIEW, uri)

        context.startActivity(sendIntent)
    }
}