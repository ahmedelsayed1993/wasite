package com.aait.wasite.UI.Controllers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.aait.wasite.Base.ParentRecyclerAdapter
import com.aait.wasite.Base.ParentRecyclerViewHolder
import com.aait.wasite.Models.DriverModel
import com.aait.wasite.Models.ServiceModel
import com.aait.wasite.R
import com.bumptech.glide.Glide

class DriverAdapter (context: Context, data: MutableList<DriverModel>, layoutId: Int) :
    ParentRecyclerAdapter<DriverModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val questionModel = data.get(position)
        viewHolder.name!!.setText(questionModel.username)
        viewHolder.location!!.setText(questionModel.address)
        viewHolder.car!!.setText(questionModel.vehicle)
        Glide.with(mcontext).load(questionModel.avatar).into(viewHolder.image)
        val animation = AnimationUtils.loadAnimation(mcontext, R.anim.item_animation_from_bottom_slow)
        animation.setDuration(750)
        viewHolder.itemView.startAnimation(animation)
        viewHolder.phone.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })
        viewHolder.whats.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)

        })




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var image = itemView.findViewById<ImageView>(R.id.image)
        internal var car = itemView.findViewById<TextView>(R.id.car)
        internal var location = itemView.findViewById<TextView>(R.id.location)
        internal var phone = itemView.findViewById<LinearLayout>(R.id.phone)
        internal var whats = itemView.findViewById<LinearLayout>(R.id.whats)



    }
}