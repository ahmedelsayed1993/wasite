package com.aait.wasite.UI.Activities.Main

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Listeners.OnItemClickListener
import com.aait.wasite.Models.*
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.LocationActivity
import com.aait.wasite.UI.Activities.LocationsActivity
import com.aait.wasite.UI.Controllers.ListAdapter
import com.aait.wasite.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestActivity:ParentActivity(),OnItemClickListener {
    override val layoutResource: Int
        get() = R.layout.activity_request

    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var type:TextView
    lateinit var type_lay:LinearLayout
    lateinit var types:RecyclerView
    lateinit var location:TextView
    lateinit var location1:TextView
    lateinit var listAdapter: ListAdapter
    lateinit var typeModel:ServiceModel
    var typeModels = ArrayList<ServiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var item:ServiceModel
    var address = ""
    var lat = ""
    var lng = ""
    var address1 = ""
    var lat1 = ""
    var lng1 = ""
    lateinit var send:Button
    override fun initializeComponents() {
        item = intent.getSerializableExtra("item") as ServiceModel
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        type = findViewById(R.id.type)
        type_lay = findViewById(R.id.type_lay)
        types = findViewById(R.id.types)
        location = findViewById(R.id.location)
        location1 = findViewById(R.id.location1)
        title.text = getString(R.string.Apply)
        send = findViewById(R.id.send)
        back.setOnClickListener { onBackPressed()
        finish()}
        listAdapter = ListAdapter(mContext,typeModels,R.layout.recycler_list)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        listAdapter.setOnItemClickListener(this)
        types.layoutManager = linearLayoutManager
        types.adapter = listAdapter
        type.setOnClickListener { getTypes() }
        location.setOnClickListener { startActivityForResult(Intent(this, LocationActivity::class.java),1)  }
        location1.setOnClickListener { startActivityForResult(Intent(this, LocationsActivity::class.java),2)  }
        send.setOnClickListener { if(
//            CommonUtil.checkTextError(type,getString(R.string.Choose_the_type_of_load))||
               CommonUtil.checkTextError(location,getString(R.string.choose_location))
//                ||
               // CommonUtil.checkTextError(location1,getString(R.string.choose_location))
        ){
            return@setOnClickListener
        }else{
            Send()
        }
        }

    }

    override fun onItemClick(view: View, position: Int) {
        typeModel = typeModels.get(position)
        type.text = typeModel.name
        type_lay.visibility = View.GONE
    }

    fun getTypes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Types("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!)?.enqueue(object:
            Callback<ServicesResponse> {
            override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                type_lay.visibility = View.GONE
            }

            override fun onResponse(
                call: Call<ServicesResponse>,
                response: Response<ServicesResponse>
            ) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        listAdapter.updateAll(response.body()?.data!!)
                        type_lay.visibility = View.VISIBLE
                    }else{
                        type_lay.visibility = View.GONE
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
          if(resultCode == 1) {
              if (data?.getStringExtra("result") != null) {
                  address = data?.getStringExtra("result").toString()
                  lat = data?.getStringExtra("lat").toString()
                  lng = data?.getStringExtra("lng").toString()
                  location.text = address
              } else {
                  address = ""
                  lat = ""
                  lng = ""
                  location.text = ""
              }
          }else{
              if (data?.getStringExtra("result") != null) {
                  address1 = data?.getStringExtra("result").toString()
                  lat1 = data?.getStringExtra("lat").toString()
                  lng1 = data?.getStringExtra("lng").toString()
                  location1.text = address1
              } else {
                  address1 = ""
                  lat1 = ""
                  lng1 = ""
                  location1.text = ""
              }
          }


    }

    fun Send(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Send("Bearer "+user.userData.token!!,lang.appLanguage,item.id!!
        ,null,lat,lng,address,null,null,null)?.enqueue(object:Callback<DriverResponse>{
            override fun onFailure(call: Call<DriverResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<DriverResponse>, response: Response<DriverResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RequestActivity,DriversActivity::class.java)
                        intent.putExtra("driver",response.body()?.data)
                      startActivity(intent)
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

}