package com.aait.wasite.UI.Activities

import android.content.Intent
import android.os.Handler
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.Auth.LoginActivity
import com.aait.wasite.UI.Activities.Main.MainActivity

class SplashActivity : ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash

    var isSplashFinishid = false
    override fun initializeComponents() {
        Handler().postDelayed({
            // logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (user.loginStatus==true){

                    val intent = Intent(this@SplashActivity, MainActivity::class.java)

                    startActivity(intent)
                    this@SplashActivity.finish()

                }else {

                    var intent = Intent(this@SplashActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }


}
