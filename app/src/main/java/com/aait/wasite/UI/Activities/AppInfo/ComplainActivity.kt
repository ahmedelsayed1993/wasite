package com.aait.wasite.UI.Activities.AppInfo

import android.content.Intent
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Models.TermsResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.Main.MainActivity
import com.aait.wasite.Utils.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ComplainActivity:ParentActivity() {
    override val layoutResource: Int
        get() = R.layout.activity_complain
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var phone:EditText
    lateinit var topic:EditText
    lateinit var text:EditText
    lateinit var send:Button

    override fun initializeComponents() {
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        phone = findViewById(R.id.phone)
        topic = findViewById(R.id.topic)
        text = findViewById(R.id.text)
        send = findViewById(R.id.send)
        title.text = getString(R.string.complaints)
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            finish()}
        send.setOnClickListener { if(CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkEditError(topic,getString(R.string.complain_title))||
                CommonUtil.checkEditError(text,getString(R.string.complain_text))){
            return@setOnClickListener
        }else{
            SendData()
        }
        }

    }

    fun SendData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Complaints(lang.appLanguage,phone.text.toString(),topic.text.toString(),text.text.toString()
        )?.enqueue(object: Callback<TermsResponse>{
            override fun onFailure(call: Call<TermsResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<TermsResponse>, response: Response<TermsResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@ComplainActivity, MainActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}