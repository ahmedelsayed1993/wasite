package com.aait.wasite.UI.Activities.Auth

import android.content.Intent
import android.widget.*
import com.aait.wasite.Base.ParentActivity
import com.aait.wasite.Models.UserResponse
import com.aait.wasite.Network.Client
import com.aait.wasite.Network.Service
import com.aait.wasite.R
import com.aait.wasite.UI.Activities.AppInfo.TermsActivity
import com.aait.wasite.Utils.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity :ParentActivity(){
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var user_name:EditText
    lateinit var phone:EditText
    lateinit var password:EditText
    lateinit var confirm_password:EditText
    lateinit var login:LinearLayout
    lateinit var register:Button
    lateinit var term:CheckBox
    lateinit var terms:TextView
    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        user_name = findViewById(R.id.user_name)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        confirm_password = findViewById(R.id.confirm_pass)
        login = findViewById(R.id.login)
        register = findViewById(R.id.register)
        term = findViewById(R.id.term)
        terms = findViewById(R.id.terms)
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java)) }
        register.setOnClickListener {
            if(CommonUtil.checkEditError(user_name,getString(R.string.user_name))||
                    CommonUtil.checkEditError(phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(password,getString(R.string.password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                CommonUtil.checkEditError(confirm_password,getString(R.string.confirm_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm_password.text.toString())) {
                    CommonUtil.makeToast(mContext, getString(R.string.password_not_match))
                }else{
                    if (term.isChecked) {
                        Register()
                    }else{
                        CommonUtil.makeToast(mContext,"يجب الموافقة على الشروط والاحكام")
                    }
                }
            }
        }


    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(user_name.text.toString(),phone.text.toString()
        ,password.text.toString(),deviceID,"android",lang.appLanguage)?.enqueue(object: Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if(response.isSuccessful){
                    if(response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,ActivationCodeActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}