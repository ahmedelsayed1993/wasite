package com.aait.wasite.Models

import java.io.Serializable

class UserModel:Serializable {
    var token:String?=null
    var name:String?=null
    var phone:String?=null

    var code:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var driver:Int?=null
    var user_type:String?=null

}