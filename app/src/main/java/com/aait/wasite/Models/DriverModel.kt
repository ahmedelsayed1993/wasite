package com.aait.wasite.Models

import java.io.Serializable

class DriverModel:Serializable {
    var avatar:String?=null
    var username:String?=null
    var vehicle:String?=null
    var address:String?=null
    var phone:String?=null
    var whatsapp:String?=null
}