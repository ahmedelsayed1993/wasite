package com.aait.wasite.Models

import android.bluetooth.BluetoothClass
import java.io.Serializable

class CommentModel:Serializable{
    var id:Int?=null
    var username:String?=null
    var comment:String?=null
    var rate:Float?=null
    var created:String?=null

}