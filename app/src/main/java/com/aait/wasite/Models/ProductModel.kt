package com.aait.wasite.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?=null
    var image:String?=null
    var title:String?=null
    var username:String?=null
    var subcategory:String?=null
    var created:String?=null
    var city:String?=null
}