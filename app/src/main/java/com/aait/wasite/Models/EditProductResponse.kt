package com.aait.wasite.Models

import java.io.Serializable

class EditProductResponse:BaseResponse(),Serializable {
    var data:EditProductModel?=null
}