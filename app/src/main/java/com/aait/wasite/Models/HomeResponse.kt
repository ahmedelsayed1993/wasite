package com.aait.wasite.Models

import java.io.Serializable

class HomeResponse:BaseResponse(),Serializable {
    var sliders:ArrayList<String>?=null
    var categories:ArrayList<CategoryModel>?=null
}