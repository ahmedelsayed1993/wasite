package com.aait.wasite.Models

import java.io.Serializable

class CategoryModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var key:String?=null
    var image:String?=null
}