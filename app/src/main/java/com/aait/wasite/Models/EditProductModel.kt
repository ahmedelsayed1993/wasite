package com.aait.wasite.Models

import java.io.Serializable

class EditProductModel:Serializable {
    var id:Int?=null
    var title_ar:String?=null
    var title_en:String?=null
    var description_ar:String?=null
    var description_en:String?=null
    var subcategory_name:String?=null
    var subcategory_id:Int?=null
    var city_name:String?=null
    var city_id:Int?=null
    var phone:String?=null
    var whatsapp:String?=null
    var category_id:Int?=null
    var images:ArrayList<ImageModel>?=null
}