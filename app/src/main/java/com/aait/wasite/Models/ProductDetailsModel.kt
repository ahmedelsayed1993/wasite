package com.aait.wasite.Models

import java.io.Serializable

class ProductDetailsModel:Serializable {
    var images:ArrayList<String>?=null
    var id:Int?=null
    var title:String?=null
    var username:String?=null
    var description:String?=null
    var phone:String?=null
    var whatsapp:String?=null
    var share_link:String?=null
    var comments:ArrayList<CommentModel>?=null
}