package com.aait.wasite.Models

import java.io.Serializable

class ServiceModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var selected:Boolean?=false

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name

    }
}