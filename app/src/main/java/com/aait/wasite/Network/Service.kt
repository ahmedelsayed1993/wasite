package com.aait.wasite.Network

import com.aait.wasite.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Service {

    @FormUrlEncoded
    @POST("sign-up")
    fun SignUp(@Field("name") name:String,
               @Field("phone") phone:String,
               @Field("password") password:String,
               @Field("device_id") device_id:String,
               @Field("device_type") device_type:String,
               @Header("lang") lang:String):Call<UserResponse>
    @FormUrlEncoded
    @POST("check-code")
    fun CheckCode(@Header("Authorization") Authorization:String,
                  @Field("code") code:String,
                  @Header("lang") lang: String):Call<UserResponse>

    @POST("resend-code")
    fun Resend(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String):Call<UserResponse>
    @FormUrlEncoded
    @POST("sign-in")
    fun Login(@Field("phone") phone:String,
              @Field("password") password:String,
              @Field("device_id") device_id:String,
              @Field("device_type") device_type:String,
              @Header("lang") lang: String):Call<UserResponse>

    @FormUrlEncoded
    @POST("forget-password")
    fun ForGot(@Field("phone") phone:String,
               @Header("lang") lang:String):Call<UserResponse>

    @FormUrlEncoded
    @POST("update-password")
    fun NewPass(@Header("Authorization") Authorization:String,
                @Field("password") password:String,
                @Field("code") code:String,
                @Header("lang") lang: String):Call<UserResponse>


    @POST("home")
    fun Home(@Header("lang") lang: String):Call<HomeResponse>


    @POST("about")
    fun About(@Header("lang") lang: String):Call<TermsResponse>
    @POST("terms")
    fun Terms(@Header("lang") lang: String):Call<TermsResponse>

    @POST("contact-us")
    fun CallUs(@Header("lang") lang:String):Call<CallUsResponse>

    @FormUrlEncoded
    @POST("complaints")
    fun Complaints(@Header("lang") lang:String,
                   @Field("phone") phone:String,
                   @Field("title") title:String,
                   @Field("complaint") complaint:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("edit-profile")
    fun Edit(@Header("Authorization") Authorization:String,
             @Header("lang") lang:String,
             @Field("name") name:String?,
             @Field("phone") phone: String?):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun AddImage(@Header("Authorization") Authorization:String,
                 @Header("lang") lang:String,
                 @Part avatar:MultipartBody.Part):Call<UserResponse>
    @FormUrlEncoded
    @POST("reset-password")
    fun resetPassword(@Header("lang") lang:String,
                      @Header("Authorization") Authorization:String,
                      @Field("current_password") current_password:String,
                      @Field("password") password:String):Call<BaseResponse>


    @POST("services")
    fun Transport(@Header("lang") lang: String):Call<ServicesResponse>


    @POST("vehicles")
    fun Vehicle(@Header("Authorization") Authorization:String,
                @Header("lang") lang: String):Call<ServicesResponse>


    @Multipart
    @POST("join-driver")
    fun Join(@Header("Authorization") Authorization:String,
             @Header("lang") lang: String,
             @Query("vehicle_id") vehicle_id:Int?,
             @Query("service_id") service_id:Int,
             @Query("lat") lat:String,
             @Query("lng") lng:String,
             @Query("address") address:String,
             @Query("phone") phone:String,
             @Query("whatsapp") whatsapp:String,
             @Part driving_license:MultipartBody.Part,
             @Part vehicle_plate:MultipartBody.Part):Call<UserResponse>

    @FormUrlEncoded
    @POST("types")
    fun Types(@Header("Authorization") Authorization:String,
              @Header("lang") lang: String,
              @Field("service_id") service_id:Int):Call<ServicesResponse>

    @FormUrlEncoded
    @POST("send-request")
    fun Send(@Header("Authorization") Authorization:String,
             @Header("lang") lang: String,
             @Field("service_id") service_id:Int,
             @Field("type_id") type_id:Int?,
             @Field("lat_from") lat_from:String,
             @Field("lng_from") lng_from:String,
             @Field("address_from") address_from:String,
             @Field("lat_to") lat_to:String?,
             @Field("lng_to") lng_to:String?,
             @Field("address_to") address_to:String?):Call<DriverResponse>
    @POST("log-out")
    fun logOut(@Header("Authorization") Authorization:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Header("lang") lang:String):Call<TermsResponse>

    @FormUrlEncoded
    @POST("subcategories")
    fun SubCat(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String,
               @Field("category_id") category_id:Int):Call<ServicesResponse>
    @POST("cities")
    fun Cities(@Header("lang") lang: String):Call<ServicesResponse>
    @FormUrlEncoded
    @POST("products")
    fun Products(@Header("Authorization") Authorization:String,
                 @Header("lang") lang: String,
                 @Field("category_id") category_id:Int,
                 @Field("subcategory_id") subcategory_id:Int?):Call<ProductsResponse>

    @FormUrlEncoded
    @POST("product-details")
    fun GetProduct(@Header("Authorization") Authorization:String,
                   @Header("lang") lang: String,
                   @Field("product_id") product_id:Int):Call<ProductDetailsResponse>

    @Multipart
    @POST("add-product")
    fun AddProduct(@Header("Authorization") Authorization:String,
                   @Header("lang") lang: String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("city_id") city_id:Int,
                   @Query("title_ar") title_ar:String,
                   @Query("title_en") title_en:String?,
                   @Query("des_ar") des_ar:String,
                   @Query("des_en") des_en:String?,
                   @Query("phone") phone:String,
                   @Query("whatsapp") whatsapp:String?,
                   @Part images:ArrayList<MultipartBody.Part>):Call<AddProductResponse>

    @POST("add-product")
    fun AddProductwithout(@Header("Authorization") Authorization:String,
                   @Header("lang") lang: String,
                   @Query("subcategory_id") subcategory_id:Int,
                   @Query("city_id") city_id:Int,
                   @Query("title_ar") title_ar:String,
                   @Query("title_en") title_en:String?,
                   @Query("des_ar") des_ar:String,
                   @Query("des_en") des_en:String?,
                   @Query("phone") phone:String,
                   @Query("whatsapp") whatsapp:String?):Call<AddProductResponse>
    @POST("my-products")
    fun MyProducts(@Header("Authorization") Authorization:String,
                   @Header("lang") lang: String):Call<ProductsResponse>

    @POST("edit-product")
    fun EditPro(@Header("Authorization") Authorization:String,
             @Header("lang") lang: String,
             @Query("product_id") product_id:Int,
             @Query("subcategory_id") subcategory_id:Int?,
                @Query("city_id") city_id:Int?,
             @Query("title_ar") title_ar:String?,
             @Query("title_en") title_en:String?,
             @Query("des_ar") des_ar:String?,
             @Query("des_en") des_en:String?,
             @Query("phone") phone:String?,
             @Query("whatsapp") whatsapp:String?,
             @Query("delete_images") delete_images:String?):Call<EditProductResponse>

    @Multipart
    @POST("edit-product")
    fun EditProduct(@Header("Authorization") Authorization:String,
                @Header("lang") lang: String,
                @Query("product_id") product_id:Int,
                @Query("subcategory_id") subcategory_id:Int?,
                    @Query("city_id") city_id:Int?,
                @Query("title_ar") title_ar:String?,
                @Query("title_en") title_en:String?,
                @Query("des_ar") des_ar:String?,
                @Query("des_en") des_en:String?,
                @Query("phone") phone:String?,
                @Query("whatsapp") whatsapp:String?,
                @Query("delete_images") delete_images:String?,
                @Part images:ArrayList<MultipartBody.Part>):Call<EditProductResponse>

    @POST("delete-product")
    fun Delete(@Header("Authorization") Authorization:String,
               @Header("lang") lang: String,
               @Query("product_id") product_id:Int):Call<TermsResponse>

    @POST("update-location")
    fun UpdateLocation(@Header("Authorization") Authorization:String,
                       @Header("lang") lang: String,
                       @Query("lat") lat:String,
                       @Query("lng") lng: String,
                       @Query("address") address: String):Call<TermsResponse>
    @POST("add-comment")
    fun AddComment(@Header("Authorization") Authorization:String,
                   @Header("lang") lang: String,
                    @Query("product_id") product_id: Int,
                   @Query("comment") comment:String,
                   @Query("rate") rate:Int):Call<TermsResponse>

}