package com.aait.wasite.GPS

interface GpsTrakerListener {
    fun onTrackerSuccess(lat: Double?, log: Double?)

    fun onStartTracker()
}
